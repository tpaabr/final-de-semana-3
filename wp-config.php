<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oT5k09HQS3yXOPZwlBKImtv/xrCDMIjj7g6c7b688uicyIYlpOBMArY6NdeDzqlbH0R2AMHME627RsDv8kAdEw==');
define('SECURE_AUTH_KEY',  'H/Ec+bZueC7+7JoNytpcjMiw5vDQvRZUnjtZMgUdyw+ESmMRGdNQJYy3I/plhjLqY8ukr6pkS0liSACf8F95+g==');
define('LOGGED_IN_KEY',    'wnCgRfyTzlGzepy0PP3iIKTpuPFEUHGoOIN8odOgwAhD4o0wFJR8/ZA5WVILfdlX7vXjx2jMxBkVsbtesq68WA==');
define('NONCE_KEY',        'uvpQnQwWGDfG92B4m9mSsMeAJE7i6hFW1ElXQCWuNXVGLB12B0TIIR8a6xhM+ij57/l0Q2wQvr+Uw5e/Q3he/w==');
define('AUTH_SALT',        'ZuD7B1uy+762g0Mq6WZwUDvZl0vp5NkwituYDkR7fcao0Gv3YaSqrEdDGlKN2NyEadB/zqBksSLiegJ19/sWSQ==');
define('SECURE_AUTH_SALT', 'k13JidmQwiJau0snzJPlJ7GYJ/mfKwWjIsnQyNz04+HjFhnc9oBC557qQmR88q5RK/6q1nIUDwN3pIAeq7NtXQ==');
define('LOGGED_IN_SALT',   'cVfaJg5DEkSYc8ZCikXNh9raFalv4ZuoUul0NJWO6i+tfC8hNP2drO4kHMW+GqSU5/FH76h7oFjd+DiUuYFwuw==');
define('NONCE_SALT',       '8BkBB5Q0aNE7M7mI7BlCeyno9/R67KCtnN3OlRvjQ8VnNjlEJLpmXZiU20YMcWSSm283JOIWaryQuEyB1R9EWg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

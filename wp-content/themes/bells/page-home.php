<?php 
//Template Name: Home
?>

<?php get_header(); ?>

    <section>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            
            <?php the_content(); ?>

        <?php endwhile; else:?>
            <p>No content</p>
        <?php  endif; ?>

    </section>

<?php get_footer(); ?>
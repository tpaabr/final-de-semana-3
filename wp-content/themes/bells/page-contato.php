<?php 
//Template Name: Contato
?>

<?php get_header(); ?>

    <section class="contato">

        <h1 id="titulo"><?php the_title(); ?></h1>

        <section id="containerFormulario">
            <div id="firstItem">

                <div class="form">
                    <p class="descForm">Nome</p>
                    <input id="inputNome" type="text">
                </div>

                <div class="form">
                    <p class="descForm">Sobrenome</p>
                    <input id="inputSobrenome" type="text">
                </div>

            </div>

            <div class="form">
                <p class="descForm">Email</p>
                <input class="inputGeral" type="text">
            </div>

            <div class="form">
                <p class="descForm">Assunto</p>
                <input class="inputGeral" type="text">
            </div>

            <div class="form">
                <p class="descForm">Mensagem</p>
                <input id="inputMensagem" type="text">
            </div>

            <div id="butEnviar">Enviar</div>

        </section>

        <section id="parceiros">

            <p class="normalText">Editoras<p>

            <div class="containerEditoras">

                <img src="<?php the_field('editora1'); ?>">
                <img src="<?php the_field('editora2'); ?>">
                <img src="<?php the_field('editora3'); ?>">

            </div>

            <p class="normalText">Parceiros<p>
            <div class="containerParceiros">

                <img src="<?php the_field('parceiro2'); ?>">
                <img src="<?php the_field('parceiro3'); ?>">

            </div>

        </section>

    </section>

<?php get_footer(); ?>
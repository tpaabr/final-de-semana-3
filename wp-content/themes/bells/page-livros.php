<?php 
//Template Name: Livros
?>

<?php get_header(); ?>

    <section class="livros">
        <h1 id="titulo"><?php the_title(); ?></h1>

        <ul class="listaLivros">
            <li class="itemLivro">
                <img src="<?php the_field('capa_livro');?>">
                <div class="info">
                    <p class="tituloLivro"><?php the_field('titulo_livro');?></p>
                    <p class="descLivro"><?php the_field('desc_livro');?></p>
                </div>
            </li>
        </ul>

    </section>

<?php get_footer(); ?>
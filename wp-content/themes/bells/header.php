<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php the_title(); ?></title>
    <link  href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" rel="stylesheet">
    <link  href="<?php echo get_stylesheet_directory_uri(); ?>/css/reset.css" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body>
    <div id="page-container">
        <div id="content-wrap">
            <header>
                <div class="mainHeader">

                    <a href="/home" class="logo">Bell's</a>

                    <div class="mainLinks">
                        <nav>
                            <a href = "/livros" class="linksHeader">Livros</a>
                            <a href = "/artes" class="linksHeader">Artes</a>
                            <a class="linksHeader">Sobre</a>
                            <a href="/contato" class="linksHeader">Contato</a>
                            <a class="linksHeader">Login</a>
                            <a class= "doar"><b>Doar</b></a>
                        </nav>
                    </div>
                </div> 
            </header>
</div>
        <footer>
            <div class="mainFooter">
                
                <div class="mainLinks">
                    
                    <nav class = "firstRow">
                        <a class="linksFooter">Sobre</a>
                        <a href="/artes" class="linksFooter">Artes</a>
                        <a href = "/artes" class="linksFooter">Livros</a>
                    </nav>

                    <nav class ="secondRow">
                        <a href="/contato" class="linksFooter">Contato</a>
                        <a class="linksFooter">Login</a>
                    </nav>
                    
                    <nav class="social">
                        <a class ="icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ant-design_instagram-outlined.png"></a>
                        <a class ="icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/ant-design_facebook-outlined.png"></a>
                        <a class ="icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cib_pinterest-p.png"></a>
                        <a class ="icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/et_twitter.png"></a>
                    </nav>
                </div>
                
                <div class="desenvolvido">
                    <p class="por">Desenvolvido por:</p>
                    <img class="logoIN" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo lINda 1.png">
                </div>

            </div>
            
            <div class="copyright">© Copyright 2020 Bell’s Books</div>
        </footer>
    </div>
    <?php wp_footer(); ?>
</body>

</html>
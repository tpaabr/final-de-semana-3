<?php 
//Template Name: Artes
?>

<?php get_header(); ?>

    <section class="artes">
        <h1 id="titulo"><?php the_title(); ?></h1>

        <div id="containerImagens">
            
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    
                    <?php the_content(); ?>
                    
                <?php endwhile; else:?>
                    <p>Nenhuma Arte</p>
                <?php  endif; ?>
            
        </div>

    </section>

<?php get_footer(); ?>